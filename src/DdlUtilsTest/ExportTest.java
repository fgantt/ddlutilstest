package DdlUtilsTest;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ddlutils.DatabaseOperationException;
import org.apache.ddlutils.Platform;
import org.apache.ddlutils.PlatformFactory;
import org.apache.ddlutils.io.DatabaseIO;
import org.apache.ddlutils.model.Column;
import org.apache.ddlutils.model.Database;
import org.apache.ddlutils.model.Table;

public class ExportTest {
	public Properties appProps;
	
	public void loadApplicationProperties() throws Exception {
		appProps = new Properties();
		appProps.load(new FileInputStream("application.properties"));
	}
	
	public DataSource getDataSource() {
	    BasicDataSource bds = new BasicDataSource();
	    bds.setDriverClassName(appProps.getProperty("jdbc.driver"));
	    bds.setUrl(appProps.getProperty("jdbc.url"));
	    bds.setUsername(appProps.getProperty("jdbc.user"));
	    bds.setPassword(appProps.getProperty("jdbc.password"));
	    return bds;
	}

	public Database getDatabase() throws DatabaseOperationException
	{
	    Platform platform = PlatformFactory.createNewPlatformInstance(getDataSource());
	    return platform.readModelFromDatabase("model");
	}
	
	public void export(DataSource dataSource, Database database) {
	    Platform  platform = PlatformFactory.createNewPlatformInstance(dataSource);
	    
	    System.out.println("============= TABLE SCHEMA DUMP =============");
	    Table[] tables = database.getTables();
	    for (Table table : tables) {
	    	Column[] columns = table.getColumns();
	    	
	    	for (Column column : columns) {
	    		System.out.println(String.format("Table=%s, Column=%s, Type=%s", table.getName(), column.getName(), column.getType()));
	    	}
	    }
	    
	    
	    System.out.println("============= TABLE DATA DUMP =============");
	    for (Table table : tables) {
	    	String query = String.format("select * from %s", table.getName());
		    Iterator it = platform.query(database, query, new Table[] { database.findTable(table.getName()) });
		    
		    for (int rownum = 1; it.hasNext(); rownum++) {
		        DynaBean row = (DynaBean)it.next();
		        System.out.println(String.format("Table=%s, Row=%d, %s", table.getName(), rownum, row.toString()));
		    }
	    }
	}
	
	public void exportSchemaSnapshot(DataSource dataSource, Database database, List tables, BufferedOutputStream out) {
		
	}
	
	public void exportDataSnapshot(DataSource dataSource, Database database, List<String> tables, FileWriter out) throws IOException {
	    Platform  platform = PlatformFactory.createNewPlatformInstance(dataSource);
	    
		for (String tableName : tables) {
			String query = String.format("select * from %s", tableName);
		    Iterator it = platform.query(database, query, new Table[] { database.findTable(tableName) });
			
		    for (int rownum = 1; it.hasNext(); rownum++) {
		        DynaBean row = (DynaBean)it.next();
		        out.write(String.format("Table=%s, Row=%d, %s\n", tableName, rownum, row.toString()));
		    }
		}
	}
	
	public void compareShemaSnapshots(File srcSnapshot, File destSnapShot, File changeSet) {
		
	}
	
	public void compareDataSnapshots() {
		
	}

	public static void main(String[] args) throws Exception {
		ExportTest et = new ExportTest();
		et.loadApplicationProperties();
		Database db = et.getDatabase();
		System.out.println(db.toString());
		
		// write db schema out to a file
		new DatabaseIO().write(db, "dbschema.xml");
		
		// test: dump db stuff
		et.export(et.getDataSource(), et.getDatabase());
		
		FileWriter fw = new FileWriter("snapshot.txt");
		et.exportDataSnapshot(et.getDataSource(), et.getDatabase(), Arrays.asList("Hierarchy", "HierarchyGroup"), fw);
		fw.close();
		
	}

}
